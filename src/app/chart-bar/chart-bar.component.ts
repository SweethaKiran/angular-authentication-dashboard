import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { UserService } from '@app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-chart-bar',
  templateUrl: './chart-bar.component.html',
  styleUrls: ['./chart-bar.component.css']
})
export class ChartBarComponent implements OnInit {

  getAllChartData: any;
  getChartBarValues: any[];
  title = 'Bar Chart';

    private width: number;
    private height: number;
    private margin = {top: 20, right: 20, bottom: 30, left: 40};

    private x: any;
    private y: any;
    private svg1: any;
    private g: any;  

  constructor(private userService: UserService) { }

  ngOnInit(): void {

    this.userService.getAll().pipe(first()).subscribe(chartData => {
      // this.loading = false;
      this.getAllChartData = chartData;
      console.log(this.getAllChartData);
      this.getChartBarValues = this.getAllChartData.chartBar;
      this.initSvg();
        this.initAxis();
        this.drawAxis();
        this.drawBars();
      
    });


  }

  private initSvg() {
    this.svg1 = d3.select('#svg1');
    this.width = +this.svg1.attr('width') - this.margin.left - this.margin.right;
    this.height = +this.svg1.attr('height') - this.margin.top - this.margin.bottom;
    this.g = this.svg1.append('g')
        .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
}

private initAxis() {
    this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(0.1);
    this.y = d3Scale.scaleLinear().rangeRound([this.height, 0]);
    this.x.domain(this.getChartBarValues.map((d) => d.name));
    this.y.domain([0, d3Array.max(this.getChartBarValues, (d) => d.value)]);
}

private drawAxis() {
    this.g.append('g')
        .attr('class', 'axis axis--x')
        .attr('transform', 'translate(0,' + this.height + ')')
        .call(d3Axis.axisBottom(this.x));
    this.g.append('g')
        .attr('class', 'axis axis--y')
        .call(d3Axis.axisLeft(this.y).ticks(10, '%'))
        .append('text')
        .attr('class', 'axis-title')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('value');
}

private drawBars() {
    this.g.selectAll('.bar')
        .data(this.getChartBarValues)
        .enter().append('rect')
        .attr('class', 'bar')
        .attr('x', (d) => this.x(d.name) )
        .attr('y', (d) => this.y(d.value) )
        .attr('width', this.x.bandwidth())
        .attr('height', (d) => this.height - this.y(d.value) );
}


}
