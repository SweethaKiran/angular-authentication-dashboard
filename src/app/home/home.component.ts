﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users: User[];
    getData: any;
    getAllChartData: any;
    getChartBarValues: any[];
    getChartDonutValues: any[];
    getTableUsersValues: any[];
    // getData: void;

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.loading = true;       
        this.userService.getAll().pipe(first()).subscribe(chartData => {
            this.loading = false;
            this.getAllChartData = chartData;
            console.log(this.getAllChartData);
            this.getValues();
        });
        
    }

    getValues(){
        this.getChartBarValues = this.getAllChartData.chartBar;
        this.getChartDonutValues = this.getAllChartData.chartDonut;
        this.getTableUsersValues = this.getAllChartData.tableUsers
        // tableUsers
        console.log(this.getChartBarValues);
        console.log(this.getChartDonutValues);
        console.log(this.getTableUsersValues);
    }
}